public class prime3{
    public static void main(String args[]) {
        int i = 2, no = 9;
        while (i < no) {
            if (no % i == 0) {
                System.out.println("It is not a prime number.");
                return;
            }
            i++;
        }
        System.out.println("It is a prime number.");
    }
}
