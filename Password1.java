public class Password1 {
    public static void main(String[] args) {
        String a = "John@123";
        int f = a.length();
        System.out.println(f);

        if (f >= 8) {
            if (a.charAt(0) >= 65 && a.charAt(0) <= 90) {
                for (int i = 1; i < a.length(); i++) {
                    char currentChar = a.charAt(i);

                    if (Character.isDigit(currentChar)) {
                        if ((int)(currentChar) >= 32 || (int)(currentChar) <= 126) {
                            System.out.println("Valid password");
                        }
                    } else {
                        System.out.println("Invalid password - must contain at least one symbol");
                        break;
                    }
                }
            } else {
                System.out.println("Invalid password - must start with a capital letter");
            }
        } else {
            System.out.println("Invalid password - length must be at least 8 characters");
        }
    }
}

