import java.util.Scanner;

class ATMPROCESS {
    static boolean check = false;
    Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        ATMPROCESS obj = new ATMPROCESS();
        System.out.println("Enter Your Card Number:");
        String cardNumber = obj.sc.nextLine(); // Read card number as a String
        obj.CardValidator(cardNumber);
        obj.Authentication();
        obj.Operations();
        System.out.println("Thank you!");
    }

    void CardValidator(String cardNumber) { // Modified method signature
        // Remove spaces and validate the length
        cardNumber = cardNumber.replaceAll("\\s", "");
        if (cardNumber.length() >= 12) {
            System.out.print("Is ");
            if (cardNumber.startsWith("2221") || cardNumber.startsWith("2720") || cardNumber.startsWith("51") || cardNumber.startsWith("55")||cardNumber.startsWith("54")) {
                System.out.println("Mastercard");
                check = true;
            } else if (cardNumber.startsWith("4")) {
                System.out.println("Visa card");
                check = true;
            } else if (cardNumber.startsWith("34") || cardNumber.startsWith("37")) {
                System.out.println("American Express");
                check = true;
            } else if (cardNumber.startsWith("5610") || cardNumber.startsWith("560221") || cardNumber.startsWith("560225")) {
                System.out.println("Bank Card");
                check = true;
            } else if (cardNumber.startsWith("5018") || cardNumber.startsWith("5020") || cardNumber.startsWith("5038") || cardNumber.startsWith("5893")
                    || cardNumber.startsWith("6304") || cardNumber.startsWith("6759") || cardNumber.startsWith("6761") || cardNumber.startsWith("6762")
                    || cardNumber.startsWith("6763")) {
                System.out.println("Maestro Card");
                check = true;
            } else if (cardNumber.startsWith("353") || cardNumber.startsWith("356")) {
                System.out.println("Rupay Card");
                check = true;
            } else if (cardNumber.startsWith("30") || cardNumber.startsWith("38")||cardNumber.startsWith("36")) {
                System.out.println("Diners Card");
                check = true;
            } else {
                System.out.println("Invalid");
                check = false;
            }
        } else {
            System.out.println("Invalid number");
        }
       
    }

    void Authentication() {
        // Authentication logic here
    }

    void Operations() {
        // Operations logic here
    }
}

