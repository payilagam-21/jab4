//import java.util.Scanner;
//public class Ano{
//
//    
//    public static void main(String[]args)
//      {
//           Scanner sc=new Scanner(System.in);//scanner Object Creation
//           Ano A=new Ano();//Class Object creation
//           int no=sc.nextInt();//input get from user
//           int add=0,sum=0;//initialized add and sum variable
//           int temp=no;int temp1=no;
//           //Armstrong number means the number count after split and multiply count the each number .
//           //after sum the product value. the total equal to the initial number is called Armstrong number
//
//           //count the number
//           int count=A.count(no);//153/3
//
//         while(temp>0){//153>0/15>0
//          //split the number
//          int split=A.split(temp);//153/3//15
//
//         //multiply the number based on count of each number.
//         int power=A.power(split,count);//3,3//27
//
//         //sum the value
//         sum=A.sum(power,add);//27,0//27
//           temp=temp/10;//153/10=15/
//          }
//         //check the number is equal is armstrong else not Armstrong no
//         System.out.println(sum==temp1?"Armstrong Number":"Not Armstrong Number");
//
//            
//          }
//
//           //counting the number in whole number method
//           int count(int no)//153
//             {
//                int count=0 ;
//                while(no>0){//153>0/15>0/1>0/0>0
//                
//                no=no/10;//153/10=15//15/10=1//1/10=0
//                count++;//1/2/3
//                }
//                return count;//3
//               
//               }
//
//          //split the numbers each by each
//          int split(int no)//153
//          {
//               int split=no%10;//153%10=3
//                no=no/10;//153/10=15
//               return split;//3
//           }
//
//         //power method is multiply the split number count times
//        int power(int no,int power)//3,3
//          {
//             int i=0,j=1;
//             while(i<power)//0<3/1<3/2<3/3<3x
//              {
//               j=j*no;//1*3=3/3*3=9/9*3=27
//               i++;//1//2//3
//               }
//               return j;//27
//            }
//
//          // sum the power value
//         int sum(int power,int sum)//27,0
//          {
//             sum=sum+power;//0+27=27
//              return sum;//27
//              }
//}
//
//
//






import java.util.Scanner;

public class Ano {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); // scanner Object Creation
        Ano A = new Ano(); // Class Object creation
        int no = sc.nextInt(); // input get from user
        int  sum = 0; // initialized add and sum variable
        int temp = no; 
        int temp1 = no;

        // Armstrong number means the number count after split and multiply count the each number.
        // after sum the product value. the total equal to the initial number is called Armstrong number

        // count the number of digits
        int count = A.count(no); //153/3

        while (temp > 0) { // 153>0/15>0
            // split the number
            int split = A.split(temp); //153/3//15

            // multiply the number based on count of each number.
            int power = A.power(split, count); // 3,3//27

            // sum the value
            sum = A.sum(power, sum); // 27,0//27
            temp = temp / 10; // 153/10=15/
        }
        // check the number is equal is armstrong else not Armstrong no
        System.out.println(sum == temp1 ? "Armstrong Number" : "Not Armstrong Number");
    }

    // counting the number in whole number method
    int count(int no) { // 153
        int count = 0;
        while (no > 0) { // 153>0/15>0/1>0/0>0
            no = no / 10; // 153/10=15//15/10=1//1/10=0
            count++; // 1/2/3
        }
        return count; // 3
    }

    // split the numbers each by each
    int split(int no) { // 153
        return no % 10; // 153%10=3
    }

    // power method is multiply the split number count times
    int power(int no, int power) { // 3,3
        int i = 0, j = 1;
        while (i < power) { // 0<3/1<3/2<3/3<3x
            j = j * no; // 1*3=3/3*3=9/9*3=27
            i++; // 1//2//3
        }
        return j; // 27
    }

    // sum the power value
    int sum(int power, int sum) { // 27,0
        sum = sum + power; // 0+27=27
        return sum; // 27
    }
}

