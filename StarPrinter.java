import java.util.Scanner;

public class StarPrinter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a number: ");
        int num = scanner.nextInt();

        if (num >= 1 && num <= 10000) {
            for (int i = 0; i < num; i++) {
                System.out.println("$");
            }
        } else {
            System.out.println("Please enter a number between 1 and 10000.");
        }
        
        scanner.close();
    }
}
