import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class URLValidatorWithExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try {
            System.out.print("Enter URL: ");
            String inputUrl = scanner.nextLine();

            validateUrl(inputUrl);
            System.out.println("URL is valid.");
        } catch (MalformedURLException e) {
            System.out.println("Invalid URL format: " + e.getMessage());
        } catch (Exception e) {
            System.out.println("An unexpected error occurred: " + e.getMessage());
        } finally {
            scanner.close();
        }
    }

    private static void validateUrl(String urlString) throws MalformedURLException {
        new URL(urlString); // This line throws MalformedURLException if the URL is not valid.
    }

}
