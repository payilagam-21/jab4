//class Insert_Position
//{
//public static void main(String[]args)
//{
//int nums[]={1,3,5,6};int target=5,i=0;
//while(i<nums.length)
//{
//if(nums[i]==target)
//{
//System.out.println(i);
//
//}
//else if(nums[i]<target)
//{
//nums[i+1]=nums[i];
//nums[i]=target;
//System.out.println(i);
//
//}
//i++;
//}
//}
//
//}
//

class Insert_Position {
    public static void main(String[] args) {
        int nums[] = {1, 3, 5, 6};
        int target = 7;
        int i = 0;

        while (i < nums.length) {
            if (nums[i] == target) {
                System.out.println(i);
                break;  // Stop the loop once the target is found
            } else if (nums[i] < target) {
                i++;
            } else {
                // If the current element is greater than the target, it means we found the correct position
                System.out.println(i);
                break;
            }
        }

        // If the target is greater than all elements, print the last index + 1
        if (i == nums.length) {
            System.out.println(i);
        }
    }
}

