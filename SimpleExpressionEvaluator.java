import java.util.Stack;

class SimpleExpressEvaluator {
    public int calculate(String s) {
        Stack<Integer> stack = new Stack<>();
        int num = 0;
        int result = 0;
        int sign = 1; // 1 for positive, -1 for negative

        for (char c : s.toCharArray()) {
            if (Character.isDigit(c)) {
                num = num * 10 + (c - '0');
            } else if (c == '+') {
                result += sign * num;
                num = 0;
                sign = 1;
            } else if (c == '-') {
                result += sign * num;
                num = 0;
                sign = -1;
            } else if (c == '(') {
                stack.push(result);
                stack.push(sign);
                result = 0;
                sign = 1;
            } else if (c == ')') {
                result += sign * num;
                num = 0;
                result *= stack.pop(); // pop sign
                result += stack.pop(); // pop previous result
            }
        }

        // Handle the last number in the expression
        result += sign * num;

        return result;
    }

    public static void main(String[] args) {
        SimpleExpressEvaluator SimpleExpressEvaluator = new SimpleExpressEvaluator();

        // Example 1
        String expression1 = "1 + 1";
        System.out.println("Example 1: " + SimpleExpressEvaluator.calculate(expression1));

        // Example 2
        String expression2 = " 2-1 + 2 ";
        System.out.println("Example 2: " + SimpleExpressEvaluator.calculate(expression2));

        // Example 3
        String expression3 = "(1+(4+5+2)-3)+(6+8)";
        System.out.println("Example 3: " + SimpleExpressEvaluator.calculate(expression3));
    }
}

//import javax.script.ScriptEngine;
//import javax.script.ScriptEngineManager;
//import javax.script.ScriptException;
//
//public class SimpleExpressionEvaluator {
//    public static void main(String[] args) {
//        // Example String
//        String expression = "(12*3)+1";
//
//        // One-liner to evaluate the expression and convert to int
//        int result = evaluateExpression(expression);
//
//        // Print the result
//        System.out.println("Result: " + result);
//    }
//
//    private static int evaluateExpression(String expression) {
//        try {
//            return (int) new ScriptEngineManager().getEngineByName("js").eval(expression);
//        } catch (ScriptException e) {
//            e.printStackTrace();
//            return 0; // Handle the exception gracefully, return default value if needed
//        }
//    }
//}
//
//
