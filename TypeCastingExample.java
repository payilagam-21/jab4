public class TypeCastingExample {
    public static void main(String[] args) {
        // Explicit Typecasting
        // Converting a string to an integer
        String numStr = "123";
        int numInt = Integer.parseInt(numStr);
        System.out.println("String: " + numStr);
        System.out.println("Integer: " + numInt);
        System.out.println("Data type of numStr: " + numStr.getClass().getSimpleName());
        System.out.println("Data type of numInt: " + ((Object)numInt).getClass().getSimpleName());

        // Converting an integer to a string
        int numInt2 = 456;
        String numStr2 = String.valueOf(numInt2);
        System.out.println("\nInteger: " + numInt2);
        System.out.println("String: " + numStr2);
        System.out.println("Data type of numInt2: " + ((Object)numInt2).getClass().getSimpleName());
        System.out.println("Data type of numStr2: " + numStr2.getClass().getSimpleName());

        // Implicit Typecasting
        // Java automatically promotes the smaller data type to the larger data type
        int a = 10;
        double b = a;
        System.out.println("\nValue of a: " + a);
        System.out.println("Value of b: " + b);
        System.out.println("Data type of a: " + ((Object)a).getClass().getSimpleName());
        System.out.println("Data type of b: " + ((Object)b).getClass().getSimpleName());
    }
}
