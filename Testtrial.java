

import java.util.Scanner;

public class Testtrial{

    public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);

        System.out.println("Enter the number of letters: ");
        int size = obj.nextInt();

        int a[] = new int[size];
        String ch3[] = new String[size];

        // Getting input for each element of the array
        for (int j = 0; j < size; j++) {
            System.out.println("Enter your element " + (j + 1) + ": ");
            ch3[j] = obj.next();
        }

        // Initializing an empty string
        String ch1 = "";

        // Getting the count for each letter and concatenating them
        for (int i = 0; i < size; i++) {
            System.out.println("Enter count for letter " + ch3[i] + ": ");
            a[i] = obj.nextInt();

            for (int j = 0; j < a[i]; j++) {
                ch1 += ch3[i];
            }
        }

        System.out.println("Resulting string: " + ch1);
    }
}

