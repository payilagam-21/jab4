import java.util.*;

class Merge_Sorted_Array {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int result[] = new int[m + n];
        for (int i = 0; i < nums1.length; i++) {
            if (nums1[i] != 0) {
                result[i] = nums1[i];
            }
        }
        int j = 0;
        for (int i = 0; i < m + n; i++) {
            if (result[i] == 0) {
                result[i] = nums2[i - (i - j)];
                j++;
            }
        }
        Arrays.sort(result);
       System.out.println(Arrays.toString(result));

//        for (int i = 0; i < m + n; i++) {
//            System.out.print(result[i] + " ");
//        }
    }

    public static void main(String[] args) {
        Merge_Sorted_Array Msa = new Merge_Sorted_Array();
        int nums1[] = {1, 2, 3, 0, 0, 0};
        int nums2[] = {2, 5, 6};
        int m = 3;
        int n = 3;
        Msa.merge(nums1, m, nums2, n);
    }
}

