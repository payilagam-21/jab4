class Typecasting
  {
   public static void main(String [] args)
    {
	  int age=18;
	  float age1=age;//downcasting//implisive//automatic//widening
	  double age2=age;//downcasting
	  System.out.println(age1);
	  System.out.println(age1);
	   
	   int no=10;
	   byte no1=(byte)no;//upcasting//explicit conversion//narrowing//manual casting
	   System.out.println(no1);
	   
	   int x=10;
	        x=x*10;
	   byte x1=10;
		   x1=(byte)(x1*10);
	   System.out.println(x);
	   System.out.println(x1);
	   
  }
}
