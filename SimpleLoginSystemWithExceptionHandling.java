import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SimpleLoginSystemWithExceptionHandling {
    private static Map<String, String> userCredentials = new HashMap<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        userCredentials.put("user1", "password1");
        userCredentials.put("user2", "password2");

        try {
            System.out.print("Enter username: ");
            String username = scanner.nextLine();

            System.out.print("Enter password: ");
            String password = scanner.nextLine();

            login(username, password);
        } catch (InvalidCredentialsException e) {
            System.out.println("Login failed: " + e.getMessage());
        } catch (Exception e) {
            System.out.println("An unexpected error occurred: " + e.getMessage());
        } finally {
            scanner.close();
        }
    }

    private static void login(String username, String password) throws InvalidCredentialsException {
        if (userCredentials.containsKey(username) && userCredentials.get(username).equals(password)) {
            System.out.println("Login successful. Welcome, " + username + "!");
        } else {
            throw new InvalidCredentialsException("Invalid username or password");
        }
    }
}

class InvalidCredentialsException extends Exception {
    public InvalidCredentialsException(String message) {
        super(message);
    }
}
