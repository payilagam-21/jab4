import java.util.Scanner;

public class Test1{
    public static void main(String[] args) {
        // Create a Scanner object to get input from the user
        Scanner scanner = new Scanner(System.in);

        // Ask the user for the size of the array
        System.out.print("Enter the size of the string array: ");
        int size = scanner.nextInt();

        // Create a string array of the specified size
        String[] stringArray = new String[size];

        // Get input for each element of the array
        for (int i = 0; i < size; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            stringArray[i] = scanner.next();
 
        }
      

        // Close the Scanner to avoid resource leaks
        scanner.close();

        // Display the elements of the array
        System.out.println("String array elements:");
        for (String element : stringArray) {
            System.out.println(element);
        }
    }
}

