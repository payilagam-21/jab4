public class ReverseInteger {
    public int reverse(int x) {
        int rev = 0;

        while (x != 0) {
            int pop = x % 10;
            x /= 10;

            // Check for overflow before updating the reversed value
            if (rev > Integer.MAX_VALUE / 10 || (rev == Integer.MAX_VALUE / 10 && pop > 8)) {
                return 0;
            }
            if (rev < Integer.MIN_VALUE / 10 || (rev == Integer.MIN_VALUE / 10 && pop < -9)) {
                return 0;
            }

            rev = rev * 10 + pop;
        }

        return rev;
    }

    public static void main(String[] args) {
        int x = 1534236469;
        ReverseInteger obj = new ReverseInteger();
        int result = obj.reverse(x);
        System.out.println(result);
    }
}


//class Solution {
//    public int reverse(int x) {
//        long rev = 0;
//        // int b = x;
//        while(x != 0){
//    
//            rev = (rev*10) + (x % 10);
//            x /= 10;
//            
//        }
//        if(rev > Integer.MAX_VALUE || rev<Integer.MIN_VALUE){
//            return 0;
//        }
//        return (int)rev;
//        
//    }
//
//}

//    public int reverse(int x) {
//        int rev=0,rem=0,temp=x,count=0;
//        while(x!=0)
//        {
//        rem=x%10;
//        rev=rev*10+rem;
//        x=x/10;
//        count++;
//        if(count==9)
//        break;
//        else
//
//        }
//        System.out.println("rev"+rev);
//        System.out.println("rem"+rem);
//        System.out.println("value"+x);
//       //System.out.println(count);
//       return rev;
//      
//    }
//
//  public static void main(String[]args)
//   {
//    int x=1534236469;//1056389759//9646324351
//    ReverseInteger obj=new ReverseInteger();
//    //int a=obj.reverse(x);
//   //System.out.println(a);
//    int rem=1%10;
//    int rev=964632435*10;
//    System.out.println(rem);
//    System.out.println(rev);
//     
//    }
//}
