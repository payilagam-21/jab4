
import java.util.ArrayList;
import java.util.Scanner;
public class Bank {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<BankAccount> accounts = new ArrayList<>();

        // Get user input for bank account details
        System.out.println("Enter the number of accounts to create:");
        int numberOfAccounts = scanner.nextInt();

        for (int i = 0; i < numberOfAccounts; i++) {
            System.out.println("Enter account number for account " + (i + 1) + ":");
            String accountNumber = scanner.next();

            System.out.println("Enter account holder name for account " + (i + 1) + ":");
            String accountHolder = scanner.next();

            System.out.println("Enter initial balance for account " + (i + 1) + ":");
            double initialBalance = scanner.nextDouble();

            // Create BankAccount object and add it to the ArrayList
            BankAccount account = new BankAccount(accountNumber, accountHolder, initialBalance);
            accounts.add(account);
        }

        // Display account details
        for (BankAccount account : accounts) {
            System.out.println(account);
            System.out.println("---------------");
        }

        // Perform actions
        System.out.println("Select an action:");
        System.out.println("1. Deposit");
        System.out.println("2. Withdraw");

        int action = scanner.nextInt();

        switch (action) {
            case 1:
                System.out.println("Enter account number to deposit:");
                String depositAccountNumber = scanner.next();
                System.out.println("Enter amount to deposit:");
                double depositAmount = scanner.nextDouble();

                for (BankAccount account : accounts) {
                    if (account.getAccountNumber().equals(depositAccountNumber)) {
                        account.deposit(depositAmount);
                        System.out.println("--------------------------------------------------");
                        System.out.println("Updated account details:");
                        System.out.println("--------------------------------------------------");
                        System.out.println(account);
                        System.out.println("--------------------------------------------------");
                        break;
                    }
                }
                break;

            case 2:
                System.out.println("Enter account number to withdraw:");
                String withdrawAccountNumber = scanner.next();
                System.out.println("Enter amount to withdraw:");
                double withdrawAmount = scanner.nextDouble();

                for (BankAccount account : accounts) {
                    if (account.getAccountNumber().equals(withdrawAccountNumber)) {
                        account.withdraw(withdrawAmount);
                        System.out.println("Updated account details:");
                        System.out.println(account);
                        break;
                    }
                }
                break;

            default:
                System.out.println("Invalid action.");
        }

        scanner.close();
    }
}

