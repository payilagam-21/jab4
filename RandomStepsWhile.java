import java.util.Random;

public class RandomStepsWhile {
    public static void main(String[] args) {
        Random random = new Random();
        int steps = 0;
        int threshold = 20;

        while (steps < threshold) {
            int randomStep = random.nextInt(10) + 1; // Generating a random number between 1 and 10
            steps += randomStep;
            System.out.println("Number of steps taken: " + steps);
        }
    }
}
