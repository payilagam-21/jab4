class FindLength {

    public static void main(String[] args) {
        FindLength l = new FindLength();
        int a[] = {1, 2, 3, 4};
        char b[]={'a','b'};
        double c[]={1.2,2.4,7.5};
        Integer size = l.sizeFind(c);
        
        System.out.println("The Array Length is " + size);
    }

    int sizeFind(int a[]) {
        int i = 0;
        
        while (true) {
            try {
                int element = a[i];
                i++;
            } catch (Exception e) {
                break;
            }
        }
        return i;
    }

     int sizeFind(char a[]) {
        int i = 0;
        
        while (true) {
            try {
                char element = a[i];
                i++;
            } catch (Exception e) {
                break;
            }
        }
        return i;
    }


    int sizeFind(double a[]) {
        int i = 0;
        
        while (true) {
            try {
                double element = a[i];
                i++;
            } catch (Exception e) {
                break;
            }
        }
        return i;
    }
}

