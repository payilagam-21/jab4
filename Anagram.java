import java.util.*;
class Anagram{
    public static void main(String[]args)
     {
        Scanner sc=new Scanner(System.in);
        String s1=sc.nextLine(),s2=sc.nextLine();
               s1=s1.toUpperCase();
               s2=s2.toUpperCase();
             if(s1.length()==s2.length())
                {
                     char ch[]=s1.toCharArray();
                     char ch1[]=s2.toCharArray();   
                     
                     Arrays.sort(ch); 
                     Arrays.sort(ch1); 
                         int count=0;
                       for(int i=0;i<ch.length;i++)
                        {
                            if(ch[i]==ch1[i])
                                {
                                 count++;
                                 }
                         }
                         if(count==ch.length)
                          {
                              System.out.println("True");
                            }
                         else
                           {
                             System.out.println("false");
                               }
                   }
             else
                  {
                    System.out.println("false");
                   }
         }

}


public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        String s1 = sc.nextLine();
        String s2 = sc.nextLine();

        sc.close();

        // Convert input strings to lowercase
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();

        if (s1.length() == s2.length()) {
            char[] ch1 = s1.toCharArray();
            char[] ch2 = s2.toCharArray();

            Arrays.sort(ch1);
            Arrays.sort(ch2);

            int count = 0;
            for (int i = 0; i < ch1.length; i++) {
                if (ch1[i] == ch2[i]) {
                    count++;
                }
            }

            if (count == ch1.length) {
                System.out.println("true");
            } else {
                System.out.println("false");
            }
        } else {
            System.out.println("false");
        }
    }


}

//import java.util.Arrays;
//
//class Anagram {
//    public static void main(String[] args) {
//        String s1 = "nagaram", s2 = "anagram";
//        s1 = s1.toUpperCase();
//        s2 = s2.toUpperCase();
//        
//        if (s1.length() == s2.length()) {
//            char[] ch1 = s1.toCharArray();
//            char[] ch2 = s2.toCharArray();
//            
//            Arrays.sort(ch1);
//            Arrays.sort(ch2);
//            
//            int count = 0;
//            for (int i = 0; i < ch1.length; i++) {
//                if (ch1[i] == ch2[i]) {
//                    count++;
//                }
//            }
//            
//            if (count == ch1.length) {
//                System.out.println("True");
//            } else {
//                System.out.println("False");
//            }
//        } else {
//            System.out.println("False");
//        }
//    }
//}

