public class PalindromeCheck {
    public static void main(String[] args) {
        String s1 = "A man, a plan, a canal: Panama";
        System.out.println(isPalindrome(s1));  // Output: true

        String s2 = "race a car";
        System.out.println(isPalindrome(s2));  // Output: false

        String s3 = " ";
        System.out.println(isPalindrome(s3));  // Output: true
    }

    public static boolean isPalindrome(String s) {
        // Remove non-alphanumeric characters and convert to lowercase
        String cleanString = s.replaceAll("[^a-zA-Z0-9]", "").toLowerCase();

        // Use two pointers to check if the string is a palindrome
        int left = 0;
        int right = cleanString.length() - 1;

        while (left < right) {
            if (cleanString.charAt(left) != cleanString.charAt(right)) {
                return false; // Characters do not match, not a palindrome
            }
            left++;
            right--;
        }

        return true; // All characters match, it's a palindrome
    }
}

