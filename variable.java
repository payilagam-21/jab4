class variable
{
static int a=15;//field-static(global static)
int b=10;//field-nonstatic(global non-static)

public static void main(String[]args)
{
variable obj=new variable();
//int a=12,b=13;//local variable
//static int c=15;//is declaration is not static local variable is only class not a method
//
//System.out.println("global Variable is:"+obj.b);//global (or) variable non static
//System.out.println("global Variable is:"+variable.a+" "+"class used");//global (or) variable static
//System.out.println("global Variable is:"+obj.a+" "+"object used");//global(or) variable static using object(static use any anywhere without any keywords but local and field variable name as same using any classname (or) object)
//System.out.println("Local Variable is:"+b);//local variable
//System.out.println("Local Variable is:"+a);//local variable
//System.out.println();

obj.primitivedatatye();
obj.non_primitive_datatype();
}

void print()
{
variable obj=new variable();
int c=obj.a=20;//this change static variable value is reflect all methods
int d=obj.b=25;//this change non static variable value is non reflect whole class.is it with in a class
System.out.println(c);
System.out.println(d);
System.out.println();
}

void print1()
{
//variable obj=new variable();
//int b=obj.a=18;
System.out.println(b);
System.out.println(a);
}

void primitivedatatye()
{
System.out.println("primitive datatypes");
int a=10;char b='A';float c=5.0f;double d=34.5643;short f=2; byte g=34; long h=48658932;boolean i=true;
System.out.println("integer "+(Integer.SIZE/8)+" BYTES. "+" Example "+a);
System.out.println("interger range "+Integer.MAX_VALUE+" to "+Integer.MIN_VALUE);
System.out.println();
System.out.println("character "+(Character.SIZE/8)+" BYTES. "+" Example "+b+" range of");
System.out.println("character range "+Character.MAX_VALUE+" to "+Character.MIN_VALUE);
System.out.println();
System.out.println("float "+(Float.SIZE/8)+" BYTES. "+" Example "+c);
System.out.println("float range "+Float.MAX_VALUE+" to "+Float.MIN_VALUE);
System.out.println();
System.out.println("double "+(Double.SIZE/8)+" BYTES. "+" Example "+d);
System.out.println("double range "+Double.MAX_VALUE+" to "+Double.MIN_VALUE);
System.out.println();
System.out.println("short "+(Short.SIZE/8)+" BYTES."+" Example "+f);
System.out.println("short range "+Short.MAX_VALUE+" to "+Short.MIN_VALUE);
System.out.println();
System.out.println("byte "+(Byte.SIZE/8)+" BYTES. "+" Example "+g);
System.out.println("Byte range "+Byte.MAX_VALUE+" to "+Byte.MIN_VALUE);
System.out.println();
System.out.println("long "+(Long.SIZE/8)+" BYTES. "+" Example "+h);
System.out.println("Long range "+Long.MAX_VALUE+" to "+Long.MIN_VALUE);
System.out.println();
//System.out.println("boolean"+a+(Boolean.SIZE/8)+"BYTES.");
}

void non_primitive_datatype()
{
System.out.println("Non primitive data types");
System.out.println("Array "+" Is stored same data types "+" Example "+"{1,2,3}"+"no range is allocated randomly in size");
System.out.println();
System.out.println();
}
}


