class ABCBAPatternRecursion {
    public static void printPattern(int rows, int currentRow, char ch) {
        if (currentRow > rows) {
            return; // Base case: stop when rows are exhausted
        }

        // Print characters in the current row
        for (int i = 1; i <= currentRow; i++) {
            System.out.print(ch);
            ch = (char) (ch >= 'A' + rows ? ch - 1 : ch + 1);
        }

        System.out.println();

        // Recursive calls to print the next rows
        printPattern(rows, currentRow + 1, ch);
        // Note: No separate loop for descending part; recursion handles it
    }

    public static void main(String[] args) {
        int rows = 5;
        printPattern(rows, 1, 'A');
    }
}

