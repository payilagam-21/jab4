import java.util.Scanner;
import java.util.InputMismatchException;


class CardValidator {
    public boolean validate(String cardNumber) {
        String cardName = getCardName(cardNumber);
        if (cardName == null) {
            System.out.println("Invalid card prefix.");
            return false;
        }

        // Remove any spaces or non-numeric characters from the card number
        cardNumber = cardNumber.replaceAll("\\s+", "");

        // Check if the card number has a valid length
        if (cardNumber.length() < 13 || cardNumber.length() > 19) {
            System.out.println("Invalid card number length.");
            return false;
        }

        // Check if the card number consists of only digits
        if (!cardNumber.matches("\\d+")) {
            System.out.println("Invalid card number format.");
            return false;
        }

        // Apply Luhn algorithm to validate the card number
        int sum = 0;
        boolean alternate = false;
        for (int i = cardNumber.length() - 1; i >= 0; i--) {
            int digit = Character.getNumericValue(cardNumber.charAt(i));
            if (alternate) {
                digit *= 2;
                if (digit > 9) {
                    digit = (digit % 10) + 1;
                }
            }
            sum += digit;
            alternate = !alternate;
        }
        if (sum % 10 == 0) {
            System.out.println("Valid " + cardName + " card number.");
            return true;
        } else {
            System.out.println("Invalid " + cardName + " card number.");
            return false;
        }
    }

    private String getCardName(String cardNumber) {
        if (cardNumber.startsWith("2221") || cardNumber.startsWith("2720") || cardNumber.startsWith("51") || cardNumber.startsWith("55")) {
            return "Mastercard";
        } else if (cardNumber.startsWith("4")) {
            return "Visa";
        } else if (cardNumber.startsWith("34") || cardNumber.startsWith("37")) {
            return "American Express";
        } else if (cardNumber.startsWith("5610") || cardNumber.startsWith("560221") || cardNumber.startsWith("560225")) {
            return "Bank";
        } else if (cardNumber.startsWith("5018") || cardNumber.startsWith("5020") || cardNumber.startsWith("5038") || cardNumber.startsWith("5893")
                || cardNumber.startsWith("6304") || cardNumber.startsWith("6759") || cardNumber.startsWith("6761") || cardNumber.startsWith("6762")
                || cardNumber.startsWith("6763")) {
            return "Maestro";
        } else if (cardNumber.startsWith("353") || cardNumber.startsWith("356")) {
            return "Rupay";
        } else if (cardNumber.startsWith("30") || cardNumber.startsWith("38")) {
            return "Diners";
        } else {
            return null;
        }
    }
}

class User {
    private int pin;
    private int balance;
    private String name;

    public User(int pin, String name) {
        this.pin = pin;
        this.name = name;
        this.balance = 10000; // Initial balance
    }

    public int getPin() {
        return pin;
    }

    public int getBalance() {
        return balance;
    }

    public void deposit(int amount) {
        balance += amount;
    }

    public boolean withdraw(int amount) {
        if (amount <= balance) {
            balance -= amount;
            return true;
        } else {
            return false; // Insufficient funds
        }
    }

    public String getName() {
        return name;
    }
}

class ATM {
    private CardValidator cardValidator;
    private User user;

    public ATM(CardValidator cardValidator) {
        this.cardValidator = cardValidator;
    }

    public void insertCard(String cardNumber) {
        if (cardValidator.validate(cardNumber)) {
            System.out.println("Card accepted.");
            authenticateUser();
        } else {
            System.out.println("Invalid card.");
        }
    }

   private void authenticateUser() {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter your PIN:");
    int pin;
    try {
        pin = scanner.nextInt();
    } catch (InputMismatchException e) {
        System.out.println("Invalid PIN format.");
        scanner.nextLine(); // Clear the input buffer
        return;
    }
    if (user != null && pin == pin) {
        System.out.println("Welcome, " + user.getName() + ".");
        showMenu();
    } else {
        System.out.println("Invalid PIN.");
    }
}


    private void showMenu() {
        Scanner scanner = new Scanner(System.in);
        boolean exit = false;
        while (!exit) {
            System.out.println("1. Check Balance");
            System.out.println("2. Deposit");
            System.out.println("3. Withdraw");
            System.out.println("4. Exit");
            System.out.print("Select an option: ");
            int choice;
            try {
                choice = scanner.nextInt();
            } catch (Exception e) {
                System.out.println("Invalid option format.");
                scanner.nextLine(); // Clear the input buffer
                continue;
            }
            switch (choice) {
                case 1:
                    System.out.println("Your balance is: $" + user.getBalance());
                    break;
                case 2:
                    System.out.print("Enter deposit amount: $");
                    int depositAmount;
                    try {
                        depositAmount = scanner.nextInt();
                    } catch (Exception e) {
                        System.out.println("Invalid amount format.");
                        scanner.nextLine(); // Clear the input buffer
                        continue;
                    }
                    user.deposit(depositAmount);
                    System.out.println("Deposit successful. Your new balance is: $" + user.getBalance());
                    break;
                case 3:
                    System.out.print("Enter withdrawal amount: $");
                    int withdrawalAmount;
                    try {
                        withdrawalAmount = scanner.nextInt();
                    } catch (Exception e) {
                        System.out.println("Invalid amount format.");
                        scanner.nextLine(); // Clear the input buffer
                        continue;
                    }
                    if (user.withdraw(withdrawalAmount)) {
                        System.out.println("Withdrawal successful. Your new balance is: $" + user.getBalance());
                    } else {
                        System.out.println("Insufficient funds.");
                    }
                    break;
                case 4:
                    System.out.println("Thank you for using the ATM. Goodbye!");
                    exit = true;
                    break;
                default:
                    System.out.println("Invalid option. Please try again.");
            }
        }
    }

    public void setUser(User user) {
        this.user = user;
    }
}

public class Atm11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        CardValidator cardValidator = new CardValidator();
        ATM atm = new ATM(cardValidator);

        System.out.print("Enter your card number: ");
        String cardNumber = scanner.nextLine();
        atm.insertCard(cardNumber);

        User user = new User(1234, "John Doe");
        atm.setUser(user);
    }
}

