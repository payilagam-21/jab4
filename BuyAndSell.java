class BuyAndSell{
 public static void main(String[]args){
      //input:[7,1,5,3,6,4]
     //output:5
     int prices[]={1,2,3,4,0,-1,8,6};
     int buy=prices[0];
     int index=0;
     for(int i=1;i<prices.length;i++){
        if(buy>=prices[i])
          {
              index=i;
              buy=prices[i];
             }
       }
        
        int profit=0;
      for(int j=index+1;j<prices.length;j++){
         
         int sell=prices[j]-buy;
          if(profit<sell)
           {
                profit=sell;
               }
      }
      System.out.println(profit);
  }
 
}




//
//import java.util.ArrayList;
//
//class Solution {
//    ArrayList<Integer> stockBuySell(int A[], int n) {
//        ArrayList<Integer> result = new ArrayList<>();
//        int buy = 0;
//        int maxProfit = 0;
//
//        for (int i = 1; i < n; i++) {
//            if (A[i] > A[buy]) {
//                int profit = A[i] - A[buy];
//                if (profit > maxProfit) {
//                    maxProfit = profit;
//                    result.clear();
//                    result.add(buy);
//                    result.add(i);
//                }
//            } else if (A[i] < A[buy]) {
//                buy = i;
//            }
//        }
//
//        result.add(maxProfit);
//        return result;
//    }
//}
//
