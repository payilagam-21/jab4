import java.util.Scanner;

class Minicalc {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the First Number");
        int firstNumber = sc.nextInt();
        System.out.println("Enter the Second Number");
        int secondNumber = sc.nextInt();
        Minicalc mc = new Minicalc();
        System.out.println("----------------------------------------------------------------");
        System.out.println("Which Operation do you want to perform (+, -, *, /, %): ");
        char operator = sc.next().charAt(0);

        switch (operator) {
            case '+':
                secondNumber = mc.add(firstNumber, secondNumber);
                break;
            case '-':
                secondNumber = mc.sub(firstNumber, secondNumber);
                break;
            case '*':
                secondNumber = mc.mul(firstNumber, secondNumber);
                break;
            case '/':
                secondNumber = mc.div(firstNumber, secondNumber);
                break;
            case '%':
                secondNumber = mc.mod(firstNumber, secondNumber);
                break;
            default:
                System.out.println("Invalid operator");
        }

        System.out.println("Your answer is: " + secondNumber);
        System.out.println("----------------------------------------------------------------");

        String option = "";
        while (!option.equalsIgnoreCase("No")) {
          
            System.out.println("Do you want to continue? (Yes/No)");
            option = sc.next();
            if (option.equalsIgnoreCase("No")) {
                System.out.println("--*Thank you welcome*--");
            } else if (option.equalsIgnoreCase("Yes")) {
                System.out.println("Enter Next Number");
                int next = sc.nextInt();
                System.out.println("Which Operation do you want to perform (+, -, *, /, %): ");
                operator = sc.next().charAt(0);
                
                switch (operator) {
                    case '+':
                        secondNumber = mc.add(secondNumber, next);
                        break;
                    case '-':
                        secondNumber = mc.sub(secondNumber, next);
                        break;
                    case '*':
                        secondNumber = mc.mul(secondNumber, next);
                        break;
                    case '/':
                        secondNumber = mc.div(secondNumber, next);
                        break;
                    case '%':
                        secondNumber = mc.mod(secondNumber, next);
                        break;
                    default:
                        System.out.println("Invalid operator");
                }
                
                System.out.println("Your answer is: " + secondNumber);
                 System.out.println("----------------------------------------------------------------");
            } else {
                System.out.println("Invalid Input");
            }
        }

       
    }

    int add(int a, int b) {
        return a + b;
    }

    int sub(int a, int b) {
        return a - b;
    }

    int mul(int a, int b) {
        return a * b;
    }

    int div(int a, int b) {
        return a / b;
    }

    int mod(int a, int b) {
        return a % b;
    }
}

 



//import java.util.Scanner;
//
//class Minicalc {
//    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//
//        System.out.println("Enter the First Number");
//        int firstNumber = sc.nextInt();
//
//        System.out.println("Enter the Second Number");
//        int secondNumber = sc.nextInt();
//
//        Minicalc mc = new Minicalc();
//
//        while (true) {
//            System.out.println("Choose an operation: +, -, *, /");
//            char operation = sc.next().charAt(0);
//
//            System.out.println("Enter Next Number");
//            int nextNumber = sc.nextInt();
//
//            switch (operation) {
//                case '+':
//                    secondNumber = mc.add(secondNumber, nextNumber);
//                    break;
//                case '-':
//                    secondNumber = mc.subtract(secondNumber, nextNumber);
//                    break;
//                case '*':
//                    secondNumber = mc.multiply(secondNumber, nextNumber);
//                    break;
//                case '/':
//                    secondNumber = mc.divide(secondNumber, nextNumber);
//                    break;
//                default:
//                    System.out.println("Invalid operation");
//            }
//
//            System.out.println("Your answer is: " + secondNumber);
//
//            System.out.println("Do you want to continue? Enter 'Yes' or 'No'");
//            String option = sc.next();
//
//            if (option.equalsIgnoreCase("No")) {
//                System.out.println("Thank you");
//                break;
//            } else if (!option.equalsIgnoreCase("Yes")) {
//                System.out.println("Invalid Input");
//                break;
//            }
//        }
//    }
//
//    int add(int a, int b) {
//        return a + b;
//    }
//
//    int subtract(int a, int b) {
//        return a - b;
//    }
//
//    int multiply(int a, int b) {
//        return a * b;
//    }
//
//    int divide(int a, int b) {
//        if (b != 0) {
//            return a / b;
//        } else {
//            System.out.println("Cannot divide by zero");
//            return a;
//        }
//    }
//}
//
