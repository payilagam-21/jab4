import java.util.Scanner;

public class Multiplychar{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the number of characters you want to input:");
        int numCharacters = scanner.nextInt();

        System.out.println("Enter your characters:");
        char[] characters = new char[numCharacters];
        for (int i = 0; i < numCharacters; i++) {
            characters[i] = scanner.next().charAt(0);
        }

        System.out.println("Enter the number of times to print:");
        int numPrints = scanner.nextInt();

        for (int i = 0; i < numCharacters; i++) {
            for (int j = 0; j < numPrints; j++) {
                System.out.print(characters[i] + " ");
            }
            System.out.println();
        }
    }
}
