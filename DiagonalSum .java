import java.util.Scanner;

public class DiagonalSum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Input the size of the matrix (N)
        int N = scanner.nextInt();

        // Calculate the sum of diagonals
        long diagonalSum = calculateDiagonalSum(N);

        // Output the result
        System.out.println(diagonalSum);
    }

    private static long calculateDiagonalSum(int N) {
        // Calculate the sum of main diagonal
        long mainDiagonalSum = (long) N * (N + 1) * (2 * N + 1) / 6;

        // Calculate the sum of anti-diagonal
        long antiDiagonalSum = (long) N * (N + 1) * (N - 1) / 3;

        // Sum of both diagonals
        return mainDiagonalSum + antiDiagonalSum;
    }
}

