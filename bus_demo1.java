import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

class bus_demo1 {
    public static void main(String[] args) {
        ArrayList<bus> buses = new ArrayList<bus>();
        ArrayList<booking> bookings = new ArrayList<booking>();

        buses.add(new bus(1, true, 3));
        buses.add(new bus(2, false, 8));
        buses.add(new bus(3, true, 4));

        int useropt = 1;
        Scanner scanner = new Scanner(System.in);

        for (bus b : buses) {
            b.displaybusinfo();
        }

        while (useropt == 1) {
            System.out.println("Enter 1 to book and 2 to exit");
            useropt = scanner.nextInt();
            if (useropt == 1) {
                booking booking1 = new booking(scanner);
                if (booking1.isAvailable(bookings, buses)) {
                    bookings.add(booking1);
                    System.out.println("Your booking is confirmed");
                } else {
                    System.out.println("Sorry, bus is full. Try another bus or date.");
                }
            }
        }
    }
}

class bus {
    private int busno;
    private boolean ac;
    private int capacity;

    bus(int no, boolean ac, int cap) {
        this.busno = no;
        this.ac = ac;
        this.capacity = cap;
    }

    public int getcapacity() {
        return capacity;
    }

    public int getbusno() {
        return busno;
    }

    public void displaybusinfo() {
        System.out.println("Bus no: " + busno + " Ac: " + ac + " Total capacity: " + capacity);
    }
}

class booking {
    String passengername;
    int busno;
    Date date;

    booking(Scanner scanner) {
        System.out.println("Enter name of passenger");
        passengername = scanner.next();
        System.out.println("Enter Bus number");
        busno = scanner.nextInt();
        System.out.println("Enter date dd-MM-yyyy");
        String dateinput = scanner.next();
        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            date = dateformat.parse(dateinput);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public boolean isAvailable(ArrayList<booking> bookings, ArrayList<bus> buses) {
        int capacity = 0;
        for (bus b : buses) {
            if (b.getbusno() == busno) {
                capacity = b.getcapacity();
            }
        }

        int booked = 0;
        for (booking b : bookings) {
            if (b.busno == busno && b.date.equals(date)) {
                booked++;
            }
        }

        return booked < capacity;
    }
}

